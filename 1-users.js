const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


// Q1 Find all users who are interested in video games.

function getUsersInterestedInVideoGames(users) {
    
    let usersIntrestedInVG = Object.entries(users).filter((each)=>{
        let interestsString = each[1].interests.join("")
        return interestsString.includes("Video Games")
    }).map((each)=>{
        return each[0]
    })

    return usersIntrestedInVG

}
//console.log(getUsersInterestedInVideoGames(users))

// Q2 Find all users staying in Germany.

function usersStayingInGermany(users) {


    let  usersInGermany = Object.entries(users).filter((each) => {
        return each[1].nationality === "Germany"
    }).map((each) => {
        return each[0]
    })
    return usersInGermany
}

//console.log(usersStayingInGermany(users))


// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10






// Q4 Find all users with masters Degree.
function usersWithMasterDegree(users) {
    let userNamesWithMasters = Object.entries(users).filter((each)=>{
        return each[1].qualification.includes("Masters")
    }).map((each)=>{
        return each[0]
    })
    return userNamesWithMasters
}
//console.log(usersWithMasterDegree(users))


// Q5 Group users based on their Programming language mentioned in their designation.

